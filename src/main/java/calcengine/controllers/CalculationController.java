package calcengine.controllers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import calcengine.dao.AssetDao;
import calcengine.domain.CashFlow;
import calcengine.domain.ChildFund;
import calcengine.domain.Greeting;
import calcengine.domain.ScfReq;
import calcengine.domain.ScfResp;
import calcengine.service.SingleFundCalcService;
import calcengine.service.SingleFundCalcServiceImpl;



@RestController
public class CalculationController {

	static Logger log = Logger.getLogger(SingleFundCalcServiceImpl.class.getName());
   
    @RequestMapping(value = "/cashflow-svc/v1/calculatebreach", method = RequestMethod.POST)
    public ScfResp calcBreachForCashFlow(@Valid @RequestBody ScfReq request)
    {
    	log.info("Entering calcBreachForCashFlow");
    	
    	CashFlow cf = new CashFlow();
    	//Input data from UI
//    	cf.getClientName();
//    	cf.getEventDate();
    	
//    	cf.getTransactionType();
    	cf.setTransactionType(request.getCashFlowIntent().getReqDetail().get(0).getTransactionType());
    	
//    	cf.getCashFlowAmount();
    	cf.setCashFlowAmount(new BigDecimal(200000000));
    	
//    	cf.getFundId();
    	cf.setFundId(request.getCashFlowIntent().getReqDetail().get(0).getFundId());
    	
    	//Input data from DB
    	//Get Invest One Number
    	
    	//Get Total Net Assets (TNA)
    	cf.setTotalNetAssets(new BigDecimal(44683271));
    	
    	/*
    	 * Get Estimated Trading Cost in bps (ETB)
    	 * Call business service and pass cf.getCashflowAmount() as the argument
    	 */
    	cf.setTradeCostBps(new BigDecimal(11.934806));
    	
    	//Get the materiality threshold for the fund
    	cf.setMaterialityThreshold(new BigDecimal(5));
    	
    	//Determine if it's a single fund or FOF (Fund of Fund)
    	
    	/*
    	 * if(singleFund) Call the SingleFundCalcService
    	 */
    	SingleFundCalcService singleFundCalcService = new SingleFundCalcServiceImpl(); 
    	//Get the Asset information for the fund from the Database
    	try {
			AssetDao asset = singleFundCalcService.getAssetInformation(cf);
			if(asset.getTotalNetAssets().compareTo(new BigDecimal(0)) == 0) {
				//Return validation message 
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    	
    	CashFlow cfRes = new CashFlow();
		try {
			cfRes = singleFundCalcService.calcSingleFundBreach(cf);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	/*
    	 * if(FOF) Call the FundOfFundCalcService
    	 */
    	//FundOfFundCalcService fofCalcService = null;
    	//fofCalcService.calcFundOfFundBreach(cf);
    	
    	/*
    	 * Sample FOF response
    	 */
    	ScfResp scfResp = new ScfResp();
    	
//    	ChildFund childFund1 = new ChildFund();
//    	List<CashFlow> cfList2 = new ArrayList<CashFlow>();
//    	CashFlow cf2 = new CashFlow();
//    	cf2.setFundId(123);
//    	cf2.setTradeCostBps(new BigDecimal(9.8));
//    	cfList2.add(cf2);
//    	childFund1.setCashFlow(cfList2);
//    	
//    	cfRes.setChildFunds(childFund1);
    	/*
    	 * Sample FOF response
    	 */
    	
    	scfResp.setCashFlow(cfRes);
    	
    	log.info("Leaving calcBreachForCashFlow");
    	  	
    	return scfResp;
    }
}
