package calcengine.service;

import calcengine.dao.AssetDao;
import calcengine.domain.CashFlow;
import calcengine.domain.TierCalc;

/**
 * @author ysable
 *
 */
public interface SingleFundCalcService {
	
	/**
	 * @param cf
	 * @return
	 * @throws Exception
	 */
	public CashFlow calcSingleFundBreach(CashFlow cf) throws Exception;
	
	/**
	 * @param cf
	 * @return
	 */
	public TierCalc calcCashFlow(CashFlow cf);
	
	/**
	 * @param cf
	 * @return
	 * @throws Exception
	 */
	public AssetDao getAssetInformation(CashFlow cf) throws Exception;
}
