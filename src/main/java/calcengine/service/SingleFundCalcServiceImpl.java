package calcengine.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import calcengine.constants.Constants.BreachConstants;
import calcengine.dao.AssetDao;
import calcengine.domain.CashFlow;
import calcengine.domain.TierCalc;

/**
 * @author ysable Business service for Single Fund breach calculation
 *
 */
@Service
public class SingleFundCalcServiceImpl implements SingleFundCalcService {

	TierCalc tierResults = new TierCalc();
	static Logger log = Logger.getLogger(SingleFundCalcServiceImpl.class.getName());

	/* (non-Javadoc)
	 * @see calcengine.service.SingleFundCalcService#calcSingleFundBreach(calcengine.domain.CashFlow)
	 */
	public CashFlow calcSingleFundBreach(CashFlow cf) {
		log.info("Entering calcSingleFundBreach");

		try {
			
			log.info("Cash Flow Amount: "+cf.getCashFlowAmount());
			//Get the fund information based on the fundId
			log.info("Fund : "+cf.getFundName());

			// Calculate Adjusted Assets (AA)
			/*
			 * For PURCHASE BigDecimal adjustedAssets = TNA +
			 * cf.getCashFlowAmount();
			 * 
			 * For SALE BigDecimal adjustedAssets = TNA;
			 */
			
			//Get Asset information from the Database
			AssetDao asset = getAssetInformation(cf);
			
			BigDecimal totalNetAssets = asset.getTotalNetAssets();
			if(totalNetAssets.compareTo(new BigDecimal(0)) == 0) {
				throw new IllegalArgumentException("Total Net Assets cannot be 0");
			}
			
			BigDecimal adjustedAssets = totalNetAssets;
			if (cf.getTransactionType().equalsIgnoreCase("PURCHASE")) {
				adjustedAssets = totalNetAssets.add(cf.getCashFlowAmount());
			}
			if(adjustedAssets.compareTo(new BigDecimal(0)) == 0) {
				throw new IllegalArgumentException("Adjusted Assets cannot be 0");
			}
			
			cf.setAdjustedAssets(adjustedAssets.setScale(2, RoundingMode.HALF_EVEN));
			log.info("Assets for Impact Measurement: " + cf.getAdjustedAssets());

			cf.setTotalNetAssetDate(asset.getTotalNetAssetDate());
			log.info("Total Net Asset Date: "+cf.getTotalNetAssetDate());
			
			// Calculate Cash Flow as % Current Assets			
			BigDecimal cashFlowTotalNetAssetsPct = (cf.getCashFlowAmount().divide(totalNetAssets, 4,
					RoundingMode.HALF_EVEN)).multiply(new BigDecimal(100));
			cf.setCashFlowTotalNetAssetsPct(cashFlowTotalNetAssetsPct.setScale(2));
			log.info("Cash Flow as % of Total Net Assets: " + cf.getCashFlowTotalNetAssetsPct());

			// Calculate Cash Flow as % Impact Assets
			BigDecimal cashFlowAdjAssetsPct = (cf.getCashFlowAmount().divide(adjustedAssets, 4, RoundingMode.HALF_EVEN))
					.multiply(new BigDecimal(100));
			cf.setCashFlowAdjAssetsPct(cashFlowAdjAssetsPct.setScale(2));
			log.info("Cash Flow as % of Impacted Assets: "+cf.getCashFlowAdjAssetsPct());

			// Calculate Trading Cost bps			
			BigDecimal tradingCostBps = asset.getTradeCostBps().setScale(7, RoundingMode.HALF_EVEN);
			cf.setTradeCostBps(tradingCostBps);
			log.info("Trading Cost (bps): "+cf.getTradeCostBps());

			// Calculate Trading Cost (ETC)
			BigDecimal tradingCost = (cf.getCashFlowAmount().multiply(cf.getTradeCostBps()))
					.divide(new BigDecimal(10000));
			cf.setTradeCost(tradingCost.setScale(2, RoundingMode.HALF_EVEN));
			log.info("Trading Cost ($$$): "+cf.getTradeCost());

			// Calculate Estimated Impact to Pool (EIP) bps
			BigDecimal impactToPool = (tradingCost.divide(adjustedAssets, 10, RoundingMode.HALF_EVEN))
					.multiply(new BigDecimal(10000));
			cf.setImpactToPoolBps(impactToPool.setScale(2, RoundingMode.HALF_EVEN));
			log.info("Estimated Impact to Pool (bps): "+cf.getImpactToPoolBps());

			// Determine breach
			BigDecimal materialityThreshold = asset.getMaterialityThreshold();
			String breach = BreachConstants.NO_BREACH.breach();
			if (impactToPool.compareTo(materialityThreshold) >= 0) {
				breach = BreachConstants.FULL_BREACH.breach();
			} else if (impactToPool.compareTo(materialityThreshold.multiply(new BigDecimal(0.5))) >= 0) {
				breach = BreachConstants.HALF_BREACH.breach();
			}
			cf.setBreach(breach);
			log.info("Materiality Determination: "+cf.getBreach());

		} catch (Exception e) {
			e.printStackTrace();
		}
		// Calculate % Cash Flow causing Full Breach

		// Calculate % Cash Flow causing Half Breach

		log.info("Leaving calcSingleFundBreach");
		return cf;
	}

	/* (non-Javadoc)
	 * @see calcengine.service.SingleFundCalcService#calcCashFlow(calcengine.domain.CashFlow)
	 */
	public TierCalc calcCashFlow(CashFlow cf) {
		// Get the trade cost (bps) for each break point of trade size
		TreeMap<BigDecimal, BigDecimal> breakPointMap = new TreeMap<BigDecimal, BigDecimal>();
		breakPointMap.put(new BigDecimal(0), new BigDecimal(8.5566815)); // 17.000000
		breakPointMap.put(new BigDecimal(150000000), new BigDecimal(10.6041735)); // 25.000000
		breakPointMap.put(new BigDecimal(300000000), new BigDecimal(12.2106485)); // 31.000000
		breakPointMap.put(new BigDecimal(500000000), new BigDecimal(13.8004015)); // 36.000000

		tierResults = calcCashFlowForBreach(cf, breakPointMap, "PURCHASE", "FULL");
		tierResults = calcCashFlowForBreach(cf, breakPointMap, "PURCHASE", "HALF");
		tierResults = calcCashFlowForBreach(cf, breakPointMap, "SALE", "FULL");
		tierResults = calcCashFlowForBreach(cf, breakPointMap, "SALE", "HALF");

		return tierResults;

	}

	/**
	 * Method to calculate minimum cash flow that would cause a full/half breach for a purchase/sale
	 * 
	 * @param cf
	 * @param breakPointMap
	 * @param transactionType
	 * @param breachType
	 * @return
	 */
	public TierCalc calcCashFlowForBreach(CashFlow cf, TreeMap<BigDecimal, BigDecimal> breakPointMap,
			String transactionType, String breachType) {

		BigDecimal threshold = cf.getMaterialityThreshold();
		if (breachType.equals("HALF")) {
			threshold = cf.getMaterialityThreshold().divide(new BigDecimal(2));
		}
		BigDecimal totalNetAssets = cf.getTotalNetAssets().setScale(2, RoundingMode.CEILING);

		BigDecimal commonDividend = threshold.multiply(totalNetAssets);
		BigDecimal minCashFlow = null;
		Boolean minCashFlowFound = false;

		Collection<BigDecimal> c = breakPointMap.keySet();
		Iterator<BigDecimal> itr = c.iterator();
		BigDecimal prevTier = itr.next();
		BigDecimal prevTradeCostBps = breakPointMap.get(prevTier);
		BigDecimal prevTradeSize = commonDividend.divide((prevTradeCostBps.subtract(threshold)), 2,
				RoundingMode.CEILING);
		while (itr.hasNext()) {

			BigDecimal currentTier = itr.next();
			BigDecimal currentTradeCostBps = breakPointMap.get(currentTier);
			BigDecimal currTradeSize = null;
			if (transactionType.equals("PURCHASE")) {
				currTradeSize = commonDividend.divide((currentTradeCostBps.subtract(threshold)), 2,
						RoundingMode.CEILING);
			}
			if (transactionType.equals("SALE")) {
				currTradeSize = commonDividend.divide(currentTradeCostBps, 2, RoundingMode.CEILING);
			}

			// Calculate Trade Size (Cash Flow) for each tier (PURCHASE)
			// For all tiers except the last one
			if (itr.hasNext()) {
				if (prevTradeSize.compareTo(currentTier) < 0 && prevTradeSize.compareTo(prevTier) >= 0) {
					minCashFlow = prevTradeSize;
					minCashFlowFound = true;
					break;
				}
			}
			// For the last tier
			else {
				if (prevTradeSize.compareTo(currentTier) < 0 && prevTradeSize.compareTo(prevTier) >= 0) {
					minCashFlow = prevTradeSize;
					minCashFlowFound = true;
					break;
				}
				if (currTradeSize.compareTo(currentTier) >= 0) {
					minCashFlow = currTradeSize;
					minCashFlowFound = true;
				}
			}

			prevTier = currentTier;
			prevTradeSize = currTradeSize;

		}

		// If none of the calculated cash flows (trade sizes) falls within its
		// respective tiers (PURCHASE)
		if (!minCashFlowFound) {
			/*
			 * Calculate the Impact to Pool for $1 below each breakpoint and get
			 * the closest one under the breach threshold
			 */

		}

		if (transactionType.equals("PURCHASE") && breachType.equals("FULL")) {
			tierResults.setMinTradeSizeFullBrPur(minCashFlow);
		}
		if (transactionType.equals("PURCHASE") && breachType.equals("HALF")) {
			tierResults.setMinTradeSizeHalfBrPur(minCashFlow);
		}
		if (transactionType.equals("SALE") && breachType.equals("FULL")) {
			tierResults.setMinTradeSizeFullBrSale(minCashFlow);
		}
		if (transactionType.equals("SALE") && breachType.equals("HALF")) {
			tierResults.setMinTradeSizeHalfBrSale(minCashFlow);
		}

		return tierResults;
	}

	/**
	 * Method to return the asset information for the given fund
	 * 
	 * @param cf
	 * @return
	 */
	public AssetDao getAssetInformation(CashFlow cf) throws Exception{
		/*
		 * TODO Replace this code with calls to Database
		 */
		AssetDao dao = new AssetDao();
		dao.setTotalNetAssets(cf.getTotalNetAssets());
		dao.setTradeCostBps(cf.getTradeCostBps());
		dao.setMaterialityThreshold(cf.getMaterialityThreshold());
		dao.setTotalNetAssetDate(cf.getTotalNetAssetDate());
		
		return dao;
	}
}
