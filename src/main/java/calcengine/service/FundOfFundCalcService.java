package calcengine.service;

import calcengine.domain.CashFlow;

/**
 * @author ysable
 * Business service for Fund of Fund breach calculation
 *
 */
public interface FundOfFundCalcService {
	
	public CashFlow calcFundOfFundBreach(CashFlow cf);

}
