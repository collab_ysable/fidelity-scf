package calcengine.service;

import calcengine.domain.CashFlow;

/**
 * @author ysable
 * Business service for Fund of Fund breach calculation
 *
 */
public class FundOfFundCalcServiceImpl implements FundOfFundCalcService {

	@Override
	public CashFlow calcFundOfFundBreach(CashFlow cf) {
		
		// Go to the database and retrieve the child funds
		
		// List<CashFlow> childFunds = DB.getChildren(cf.getFundId());
		
		
		// For each child fund, calculate the percentage of the child fund.
		
		// 		childPercentage = child.ComponentMktValue / cf.TNA
		
		
		// For each child fund, calculate the cash flow into the child fund.
		
		// 		childCashFlow = cf.getCashFlowAmount() * childPercentage
					
		// For each child fund, if the fund has a full breach, calculate the breach amount
		
		// List<CashFlow> childBreaches = new List<CashFlow>();		
		 
		// for each childCashFlow,
		// 		childBreaches.add(SingleFundCalcServiceImpl.calcSingleFundBreach(childCashFlow));
		
		
		// Sum the breach amounts.
		
		// for each child in childBreaches
		//		totalCashBreaches += child.getTradeImpacts()
		
		// return totalCashBreaches
		
		return cf;
	}
		

}
