/**
 * 
 */
package calcengine.dao;

/**
 * @author shazlehurst
 *
 */

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface DataAccess {

	public BigDecimal getTotalNetAssets(int fundID) throws DataAccessException;	
	public BigDecimal getMaterialityThreshold(int fundID) throws DataAccessException;
	public BigDecimal getTradeCostsInBps(int fundID, BigDecimal cashFlow) throws DataAccessException;
	public Date getTotalNetAssetsDate(int fundID) throws DataAccessException;
	
	public List<TradeCostTier> getTradeCostTiers(int fundID) throws DataAccessException;
		
	public void releaseConnection() throws DataAccessException;	
}
