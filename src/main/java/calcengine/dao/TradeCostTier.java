/**
 *  This class represents a trade tier / trade cost pair, e.g. For a $150,000,000 the trade cost is 7.3243 bps
 */
package calcengine.dao;

/**
 * @author shazlehurst
 *
 */


import java.math.BigDecimal;


public class TradeCostTier {

	private BigDecimal tierInDollars;
	private BigDecimal costInBps;
	
	/**
	 * @param tierInDollars
	 * @param costInBps
	 */
	public TradeCostTier(BigDecimal tierInDollars, BigDecimal costInBps) {
		this.tierInDollars = tierInDollars;
		this.costInBps = costInBps;
	}
	
	@SuppressWarnings("unused")
	private TradeCostTier() {
		// prevent instantiation of invalid TradeCost object
	}
	
	public BigDecimal getCostInBps() {
		return costInBps;
	}
	
	public BigDecimal getTierInDollars() {
		return tierInDollars;
	}

}
