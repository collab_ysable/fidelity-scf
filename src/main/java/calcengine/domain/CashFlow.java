package calcengine.domain;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

public class CashFlow {

	private ChildFund childFunds;
	
	public ChildFund getChildFunds() {
		return childFunds;
	}

	public void setChildFunds(ChildFund childFunds) {
		this.childFunds = childFunds;
	}

	private Integer scfReqId;
	private Integer clientId;
	private String clientName;
	private String prospectName;
	private Date eventDate;
	private List<TargetDateParam> tgtDateParamList;
	
	public List<TargetDateParam> getTgtDateParamList() {
		return tgtDateParamList;
	}

	public void setTgtDateParamList(List<TargetDateParam> tgtDateParamList) {
		this.tgtDateParamList = tgtDateParamList;
	}
	
	public Integer getScfReqId() {
		return scfReqId;
	}

	public void setScfReqId(Integer scfReqId) {
		this.scfReqId = scfReqId;
	}

	@NotNull
	private BigDecimal cashFlowAmount;
	
	@NotNull
	private Integer fundId;
	
	@NotNull
	private String fundName;
	
	@NotNull 
	private String transactionType;
	
	private BigDecimal totalNetAssets;
	private BigDecimal tradeCostBps;
	private BigDecimal tradeCost;
	private BigDecimal materialityThreshold;
	private BigDecimal impactToPoolBps;
	private Date navDate;
	private BigDecimal cashFlowTotalNetAssetsPct;
	private BigDecimal cashFlowAdjAssetsPct;
	private String breach;
	private BigDecimal pctCashFlowFullBreach;
	private BigDecimal pctCashFlowHalfBreach;
	private BigDecimal adjustedAssets;	
	private Date totalNetAssetDate;
	private Date tradeCostDate;
	private Date materialityThresholdDate;
	
	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getProspectName() {
		return prospectName;
	}

	public void setProspectName(String prospectName) {
		this.prospectName = prospectName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public BigDecimal getCashFlowAmount() {
		return cashFlowAmount;
	}

	public void setCashFlowAmount(BigDecimal cashFlowAmount) {
		this.cashFlowAmount = cashFlowAmount;
	}

	public Integer getFundId() {
		return fundId;
	}

	public void setFundId(Integer fundId) {
		this.fundId = fundId;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public BigDecimal getTotalNetAssets() {
		return totalNetAssets;
	}

	public void setTotalNetAssets(BigDecimal totalNetAssets) {
		this.totalNetAssets = totalNetAssets;
	}

	public BigDecimal getTradeCostBps() {
		return tradeCostBps;
	}

	public void setTradeCostBps(BigDecimal tradeCostBps) {
		this.tradeCostBps = tradeCostBps;
	}

	public BigDecimal getTradeCost() {
		return tradeCost;
	}

	public void setTradeCost(BigDecimal tradeCost) {
		this.tradeCost = tradeCost;
	}

	public BigDecimal getMaterialityThreshold() {
		return materialityThreshold;
	}

	public void setMaterialityThreshold(BigDecimal materialityThreshold) {
		this.materialityThreshold = materialityThreshold;
	}

	public BigDecimal getImpactToPoolBps() {
		return impactToPoolBps;
	}

	public void setImpactToPoolBps(BigDecimal impactToPoolBps) {
		this.impactToPoolBps = impactToPoolBps;
	}

	public Date getNavDate() {
		return navDate;
	}

	public void setNavDate(Date navDate) {
		this.navDate = navDate;
	}

	public BigDecimal getCashFlowTotalNetAssetsPct() {
		return cashFlowTotalNetAssetsPct;
	}

	public void setCashFlowTotalNetAssetsPct(BigDecimal cashFlowTotalNetAssetsPct) {
		this.cashFlowTotalNetAssetsPct = cashFlowTotalNetAssetsPct;
	}

	public BigDecimal getCashFlowAdjAssetsPct() {
		return cashFlowAdjAssetsPct;
	}

	public void setCashFlowAdjAssetsPct(BigDecimal cashFlowAdjAssetsPct) {
		this.cashFlowAdjAssetsPct = cashFlowAdjAssetsPct;
	}

	public String getBreach() {
		return breach;
	}

	public void setBreach(String breach) {
		this.breach = breach;
	}

	public BigDecimal getPctCashFlowFullBreach() {
		return pctCashFlowFullBreach;
	}

	public void setPctCashFlowFullBreach(BigDecimal pctCashFlowFullBreach) {
		this.pctCashFlowFullBreach = pctCashFlowFullBreach;
	}

	public BigDecimal getPctCashFlowHalfBreach() {
		return pctCashFlowHalfBreach;
	}

	public void setPctCashFlowHalfBreach(BigDecimal pctCashFlowHalfBreach) {
		this.pctCashFlowHalfBreach = pctCashFlowHalfBreach;
	}

	public BigDecimal getAdjustedAssets() {
		return adjustedAssets;
	}

	public void setAdjustedAssets(BigDecimal adjustedAssets) {
		this.adjustedAssets = adjustedAssets;
	}

	public Date getTotalNetAssetDate() {
		return totalNetAssetDate;
	}

	public void setTotalNetAssetDate(Date totalNetAssetDate) {
		this.totalNetAssetDate = totalNetAssetDate;
	}

	public Date getTradeCostDate() {
		return tradeCostDate;
	}

	public void setTradeCostDate(Date tradeCostDate) {
		this.tradeCostDate = tradeCostDate;
	}

	public Date getMaterialityThresholdDate() {
		return materialityThresholdDate;
	}

	public void setMaterialityThresholdDate(Date materialityThresholdDate) {
		this.materialityThresholdDate = materialityThresholdDate;
	}
	
	
	
	

}
