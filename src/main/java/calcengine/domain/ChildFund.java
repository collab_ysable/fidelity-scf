package calcengine.domain;

import java.util.List;

public class ChildFund {
	
	private List<CashFlow> cashFlow;

	public List<CashFlow> getCashFlow() {
		return cashFlow;
	}

	public void setCashFlow(List<CashFlow> cashFlow) {
		this.cashFlow = cashFlow;
	}

}
