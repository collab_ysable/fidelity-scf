package calcengine.domain;

public class ScfReq {
	
	private CashFlowIntent cashFlowIntent;

	public CashFlowIntent getCashFlowIntent() {
		return cashFlowIntent;
	}

	public void setCashFlowIntent(CashFlowIntent cashFlowIntent) {
		this.cashFlowIntent = cashFlowIntent;
	}
}
