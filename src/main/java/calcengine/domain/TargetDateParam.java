package calcengine.domain;

public class TargetDateParam {
	
	private Integer fundId;
	private String vintage;
	
	public Integer getFundId() {
		return fundId;
	}
	public void setFundId(Integer fundId) {
		this.fundId = fundId;
	}
	public String getVintage() {
		return vintage;
	}
	public void setVintage(String vintage) {
		this.vintage = vintage;
	}

}
