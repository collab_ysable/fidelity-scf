package calcengine.domain;

public class ScfResp {
	
	private CashFlow cashFlow;

	public CashFlow getCashFlow() {
		return cashFlow;
	}

	public void setCashFlow(CashFlow cashFlow) {
		this.cashFlow = cashFlow;
	}

}
