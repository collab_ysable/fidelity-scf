package calcengine.domain;

import java.util.List;

public class CashFlowIntent {
	
	private String reqType;	
	private List<CashFlow> reqDetail;
	
	public String getReqType() {
		return reqType;
	}
	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
	public List<CashFlow> getReqDetail() {
		return reqDetail;
	}
	public void setReqDetail(List<CashFlow> reqDetail) {
		this.reqDetail = reqDetail;
	}

}
