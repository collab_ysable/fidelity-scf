package calcengine.domain;

import java.math.BigDecimal;

public class TierCalc {
	
	private BigDecimal minTradeSizeFullBrPur;
	private BigDecimal minTradeSizeFullBrSale;
	private BigDecimal minTradeSizeHalfBrPur;
	private BigDecimal minTradeSizeHalfBrSale;
	public BigDecimal getMinTradeSizeFullBrPur() {
		return minTradeSizeFullBrPur;
	}
	public void setMinTradeSizeFullBrPur(BigDecimal minTradeSizeFullBrPur) {
		this.minTradeSizeFullBrPur = minTradeSizeFullBrPur;
	}
	public BigDecimal getMinTradeSizeFullBrSale() {
		return minTradeSizeFullBrSale;
	}
	public void setMinTradeSizeFullBrSale(BigDecimal minTradeSizeFullBrSale) {
		this.minTradeSizeFullBrSale = minTradeSizeFullBrSale;
	}
	public BigDecimal getMinTradeSizeHalfBrPur() {
		return minTradeSizeHalfBrPur;
	}
	public void setMinTradeSizeHalfBrPur(BigDecimal minTradeSizeHalfBrPur) {
		this.minTradeSizeHalfBrPur = minTradeSizeHalfBrPur;
	}
	public BigDecimal getMinTradeSizeHalfBrSale() {
		return minTradeSizeHalfBrSale;
	}
	public void setMinTradeSizeHalfBrSale(BigDecimal minTradeSizeHalfBrSale) {
		this.minTradeSizeHalfBrSale = minTradeSizeHalfBrSale;
	}
	
	

}
