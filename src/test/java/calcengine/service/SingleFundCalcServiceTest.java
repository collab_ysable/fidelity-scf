package calcengine.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import calcengine.controllers.Application;
import calcengine.domain.CashFlow;
import calcengine.domain.TierCalc;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class SingleFundCalcServiceTest {

	// @Autowired
	// private SingleFundCalcServiceImpl service;

	SingleFundCalcServiceImpl service = new SingleFundCalcServiceImpl();

	@Test
	public void testCalcCashFlowForFullBreach() {

		CashFlow cf = new CashFlow();
		cf.setMaterialityThreshold(new BigDecimal(5));// 4
		cf.setTotalNetAssets(new BigDecimal(1008488174.03)); // 1347886035.55

		TierCalc tierCalc = service.calcCashFlow(cf);
		System.out.println("Min CashFlow Full Breach Purchase: " + tierCalc.getMinTradeSizeFullBrPur());
		System.out.println("Min CashFlow Full Breach Sale: " + tierCalc.getMinTradeSizeFullBrSale());
		System.out.println("Min CashFlow Half Breach Purchase: " + tierCalc.getMinTradeSizeHalfBrPur());
		System.out.println("Min CashFlow Half Breach Sale: " + tierCalc.getMinTradeSizeHalfBrSale());

	}

	// Read a CSV file
	@Test
	public void testCalcSingleFundBreach() {

		// Read test data from CSV file
		String csvFile = "C:\\Fidelity-SCF\\Test\\Test_Data.csv";
		String line = "";
		String csvSplitBy = ",";

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			br.readLine();

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] data = line.split(csvSplitBy);

				CashFlow cf = new CashFlow();

				cf.setCashFlowAmount(new BigDecimal(data[1]));
				cf.setTransactionType(data[0]);
				cf.setFundId(Integer.parseInt(data[4]));
				cf.setFundName(data[3]);
				cf.setTotalNetAssets(new BigDecimal(data[6]));
				cf.setTradeCostBps(new BigDecimal(data[10]));
				cf.setMaterialityThreshold(new BigDecimal(data[13]));

				service.calcSingleFundBreach(cf);

				System.out.println("Total Net Assets: " + cf.getTotalNetAssets());
				Assert.assertEquals(new BigDecimal(data[6]), cf.getTotalNetAssets());

				System.out.println("Assets for Impact Measurement: " + cf.getAdjustedAssets());
				Assert.assertEquals(new BigDecimal(data[7]).setScale(2), cf.getAdjustedAssets());

				System.out.println("Cash Flow as % of Current Assets: " + cf.getCashFlowTotalNetAssetsPct());
				Assert.assertEquals(new BigDecimal(data[8].replaceAll("%", "")), cf.getCashFlowTotalNetAssetsPct());

				System.out.println("Cash Flow as % of Impact Assets: " + cf.getCashFlowAdjAssetsPct());
				Assert.assertEquals(new BigDecimal(data[9].replaceAll("%", "")), cf.getCashFlowAdjAssetsPct());

				System.out.println("Estimated Trading Cost bps: " + cf.getTradeCostBps());
				Assert.assertEquals(new BigDecimal(data[10]).setScale(7), cf.getTradeCostBps());

				System.out.println("Estimated Trading Costs: " + cf.getTradeCost());
				Assert.assertEquals(new BigDecimal(data[11]).setScale(2), cf.getTradeCost());

				System.out.println("Estimated Impact to Pool: " + cf.getImpactToPoolBps());
				Assert.assertEquals(new BigDecimal(data[12]).setScale(2), cf.getImpactToPoolBps());

				System.out.println("Materiality Threshold: " + cf.getMaterialityThreshold());
				Assert.assertEquals(new BigDecimal(data[13]), cf.getMaterialityThreshold());

				System.out.println("Materiality Determination: " + cf.getBreach());
				Assert.assertEquals(data[14], cf.getBreach());

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// Read an Excel file
	@Test
	public void testCalcSingleFundBreachExcel() {

		try {
			// Read test data from XLS file
			FileInputStream file = new FileInputStream(new File("C:\\Fidelity-SCF\\Test\\Test_Data_Excel.xls"));

			// Get the workbook instance for XLS file
			HSSFWorkbook workbook = new HSSFWorkbook(file);

			// Get first sheet from the workbook
			HSSFSheet sheet = workbook.getSheetAt(0);

			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if(row.getCell(0)==null) {
					break;
				}

				CashFlow cf = new CashFlow();

				Cell cell0 = row.getCell(0);
				cf.setTransactionType(cell0.getStringCellValue());

				Cell cell1 = row.getCell(1);
				cf.setCashFlowAmount(new BigDecimal(cell1.getNumericCellValue()));

				Cell cell3 = row.getCell(3);
				cf.setFundName(cell3.getStringCellValue());

				Cell cell4 = row.getCell(4);
				cf.setFundId(Double.valueOf(cell4.getNumericCellValue()).intValue());
				
				Cell cell5 = row.getCell(5);
				cf.setTotalNetAssetDate(new java.sql.Date(cell5.getDateCellValue().getTime()));

				Cell cell6 = row.getCell(6);
				cf.setTotalNetAssets(new BigDecimal(cell6.getNumericCellValue()));

				Cell cell7 = row.getCell(7);
				Cell cell8 = row.getCell(8);
				Cell cell9 = row.getCell(9);

				Cell cell10 = row.getCell(10);
				cf.setTradeCostBps(new BigDecimal(cell10.getNumericCellValue()));

				Cell cell11 = row.getCell(11);
				Cell cell12 = row.getCell(12);

				Cell cell13 = row.getCell(13);
				cf.setMaterialityThreshold(new BigDecimal(cell13.getNumericCellValue()));

				Cell cell14 = row.getCell(14);

				CashFlow calcResponse = service.calcSingleFundBreach(cf);

				System.out.println("Total Net Assets: " + calcResponse.getTotalNetAssets());
				Assert.assertEquals(new BigDecimal(cell6.getNumericCellValue()), calcResponse.getTotalNetAssets());
				
				System.out.println("Total Net Asset Date: " + calcResponse.getTotalNetAssetDate());

				System.out.println("Assets for Impact Measurement: " + calcResponse.getAdjustedAssets());
				Assert.assertEquals(new BigDecimal(cell7.getNumericCellValue()).setScale(2,RoundingMode.HALF_EVEN), calcResponse.getAdjustedAssets());

				System.out.println("Cash Flow as % of Current Assets: " + calcResponse.getCashFlowTotalNetAssetsPct());
				Assert.assertEquals(new BigDecimal(cell8.getNumericCellValue()).multiply(new BigDecimal(100))
						.setScale(2, RoundingMode.HALF_EVEN), calcResponse.getCashFlowTotalNetAssetsPct());

				System.out.println("Cash Flow as % of Impact Assets: " + calcResponse.getCashFlowAdjAssetsPct());
				Assert.assertEquals(new BigDecimal(cell9.getNumericCellValue()).multiply(new BigDecimal(100))
						.setScale(2, RoundingMode.HALF_EVEN), calcResponse.getCashFlowAdjAssetsPct());

				System.out.println("Estimated Trading Cost bps: " + calcResponse.getTradeCostBps());
				Assert.assertEquals(new BigDecimal(cell10.getNumericCellValue()).setScale(7,RoundingMode.HALF_EVEN), calcResponse.getTradeCostBps());

				System.out.println("Estimated Trading Costs: " + calcResponse.getTradeCost());
				Assert.assertEquals(new BigDecimal(cell11.getNumericCellValue()).setScale(2, RoundingMode.HALF_EVEN), calcResponse.getTradeCost());

				System.out.println("Estimated Impact to Pool: " + calcResponse.getImpactToPoolBps());
				Assert.assertEquals(new BigDecimal(cell12.getNumericCellValue()).setScale(2, RoundingMode.HALF_EVEN), calcResponse.getImpactToPoolBps());

				System.out.println("Materiality Threshold: " + calcResponse.getMaterialityThreshold());
				Assert.assertEquals(new BigDecimal(cell13.getNumericCellValue()), calcResponse.getMaterialityThreshold());

				System.out.println("Materiality Determination: " + calcResponse.getBreach());
				Assert.assertEquals(cell14.getStringCellValue(), calcResponse.getBreach());
			}

			file.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
